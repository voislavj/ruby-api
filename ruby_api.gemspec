# frozen_string_literal: true

require_relative 'lib/ruby_api'

Gem::Specification.new do |s|
  s.name        = 'ruby-api'
  s.version     = RubyAPI::VERSION
  s.licenses    = ['MIT']
  s.summary     = 'summary'
  s.description = 'ruby api framework'
  s.authors     = ['Voislav Jovanovic']
  s.email       = 'voislavj@gmail.com'
  s.homepage    = 'http://bitbucket.org/voislavj/ruby-api'

  s.files       = Dir['lib/**/*.rb']
  s.executables << 'rapi'

  s.add_dependency 'activesupport', '~> 5.2'
  s.add_dependency 'dry-struct', '~> 0.5'
  s.add_dependency 'dry-validation', '~> 0.12'
  s.add_dependency 'json', '~> 2.1'
  s.add_dependency 'rack', '~> 2.0'
  s.add_dependency 'thor', '~> 0.20'
  s.add_dependency 'trailblazer-operation', '~> 0.4'
end
