# frozen_string_literal: true

module RubyAPI
  RSpec.describe Config do
    describe '.from_file' do
      let(:path) { 'foo/bar/buzz' }
      subject { described_class.from_file path }
      before { allow(YAML).to receive(:load_file).with(path) { {} } }
      it { is_expected.to be_a described_class }
    end

    describe 'initialize' do
      let(:data) { {} }
      subject { described_class.new data }
      context 'success' do
        it { is_expected.to be_a described_class }
      end
    end

    describe 'merge!' do
      let(:data) { { foo: :bar } }
      let(:data_to_merge) { { foo: :buzz } }
      let(:config) { described_class.new data }
      let(:subject) { config.merge! data_to_merge }
      it { is_expected.to eq data.merge(data_to_merge) }
    end

    describe 'key?' do
      let(:data) { { foo: :bar } }
      let(:key) { :foo }
      let(:config) { described_class.new data }
      let(:subject) { config.key? key }

      context 'exist' do
        it { is_expected.to be true }
      end
      context 'missing' do
        let(:key) { :bar }
        it { is_expected.to be false }
      end
    end

    describe 'responds to data keys' do
      let(:data) { { foo: :bar } }
      let(:key) { :foo }
      let(:config) { described_class.new data }
      let(:subject) { config.send(key) }

      context 'success' do
        it { is_expected.to eq data[key] }
      end
      context 'failure' do
        let(:key) { :missingkey }
        let(:error) { /undefined method `#{key}'/ }
        it { expect { subject }.to raise_error NoMethodError, error }
      end
    end
  end
end
