# frozen_string_literal: true

module RubyAPI
  RSpec.describe Application do
    let(:root) { '/path/to/root' }
    let(:log_data) { { info: '/dev/null', error: '/dev/null' } }

    describe 'class' do
      context 'constants' do
        it { expect(described_class::APP_MODULE_NAME).to eq 'APP' }
        it { expect(described_class::APP_SOURCE_PATH).to eq 'src/main' }
      end
    end

    describe 'initialize' do
      let(:config) { Config.new(routes: {}) }
      subject { described_class.new config }

      context 'success' do
        it { is_expected.to be_a described_class }
      end

      context 'fails when configuration is not Config' do
        let(:config) { :config }
        let(:error) { "Invalid config: #{config.inspect}" }
        it { expect { subject }.to raise_error StandardError, error }
      end

      context 'fails when config.routes not being a Hash nor String' do
        let(:config) { Config.new(routes: :routes) }
        let(:error) { "Invalid routes config: #{config.routes.inspect}" }
        it { expect { subject }.to raise_error StandardError, error }
      end
    end

    describe 'boot' do
      let(:config) { Config.new({}) }
      let(:app) { described_class.new config }
      before do
        RubyAPI.root = root
        allow_any_instance_of(described_class)
          .to receive(:require)
          .with(RubyAPI.path(described_class::APP_SOURCE_PATH))
      end
      subject { app.boot }

      context 'default' do
        it { is_expected.to be_a described_class }
      end
      context 'with boot script' do
        let(:boot_script) { 'boot' }
        let(:boot_script_path) { RubyAPI.path(boot_script) }
        let(:config) { Config.new(boot_script: boot_script) }
        before do
          allow_any_instance_of(described_class)
            .to receive(:require)
            .with(boot_script_path)
          allow(File)
            .to receive(:exist?)
            .with(boot_script_path)
            .and_return(true)
        end
        it { is_expected.to be_a described_class }
      end

      context 'with missing boot script' do
        let(:boot_script) { 'boot' }
        let(:boot_script_path) { RubyAPI.path(boot_script) }
        let(:config) { Config.new(boot_script: boot_script, log: log_data) }
        before do
          allow(File)
            .to receive(:exist?)
            .with(boot_script_path)
            .and_return(false)
        end
        it { is_expected.to be_a described_class }
      end
    end

    describe 'call' do
      let(:config) { Config.new(routes: { root: 'foo/bar' }) }
      let(:app) { described_class.new(config).boot }
      let(:foo) do
        double(:foo).tap do |foo|
          allow(foo).to receive(:const_defined?).with('Bar') { true }
          allow(foo).to receive(:const_get).with('Bar') { bar }
        end
      end
      let(:bar) do
        double(:bar).tap do |bar|
          allow(bar).to receive(:process) { result }
        end
      end
      let(:app_mod) do
        double(:app_mod).tap do |app_mod|
          allow(app_mod).to receive(:const_defined?).with('Foo') { true }
          allow(app_mod).to receive(:const_get).with('Foo') { foo }
        end
      end
      let(:env) { {} }
      let(:result_success) { true }
      let(:result_data) { {} }
      let(:result) do
        Trailblazer::Operation::Railway::Result.new(
          result_success, result_data, {}
        )
      end

      subject { app.call(env) }
      before do
        RubyAPI.root = root
        allow(Kernel)
          .to receive(:const_get)
          .with(described_class::APP_MODULE_NAME)
          .and_return(app_mod)
        allow_any_instance_of(described_class)
          .to receive(:require)
          .with(RubyAPI.path(described_class::APP_SOURCE_PATH))
      end

      context 'success' do
        it { is_expected.to be_a Array }
      end

      context 'fails with expected error' do
        let(:config) { Config.new(routes: { root: 'foo/bar' }, log: log_data) }
        let(:result_success) { false }
        it { is_expected.to be_a Array }
      end

      context 'fails with unexpected error' do
        let(:config) { Config.new(log: log_data) }
        it { is_expected.to be_a Array }
      end
    end
  end
end
