# frozen_string_literal: true

module RubyAPI
  RSpec.describe Request do
    let(:app) do
      double(:app).tap do |app|
        allow(app).to receive(:app_module).with(no_args) { app_mod }
      end
    end
    let(:app_mod) do
      double(:app_mod).tap do |app_mod|
        allow(app_mod).to receive(:const_defined?).with('Foo') { foo_defined }
        allow(app_mod).to receive(:const_get).with('Foo') { foo }
      end
    end
    let(:foo_defined) { true }
    let(:foo) do
      double(:foo).tap do |foo|
        allow(foo).to receive(:const_defined?).with('Bar') { bar_defined }
        allow(foo).to receive(:const_get).with('Bar') { bar }
        allow(foo).to receive(:name).with(no_args) { 'Foo' }
      end
    end
    let(:bar) { double(:bar) }
    let(:bar_defined) { true }
    let(:body) { Tempfile.new }
    let(:env) { { 'rack.input' => body } }
    let(:routes) { { root: 'foo/bar' } }
    let(:request) { described_class.new app, env, routes }

    describe 'initialize' do
      subject { request }
      it { is_expected.to be_a described_class }
    end

    describe 'feature' do
      subject { request.feature }

      context 'success' do
        it { expect { subject }.not_to raise_error }
      end

      context 'failure' do
        let(:foo_defined) { false }
        let(:error) { 'Feature `foo` not found' }
        it { expect { subject }.to raise_error StandardError, error }
      end
    end

    describe 'operation' do
      subject { request.operation }

      context 'success' do
        it { expect { subject }.not_to raise_error }
      end

      context 'failure' do
        let(:bar_defined) { false }
        let(:error) { 'Operation `foo/bar` not found' }
        it { expect { subject }.to raise_error StandardError, error }
      end
    end

    describe 'arguments' do
      subject { request.arguments }
      it { is_expected.to eq({}) }
    end

    describe 'route from routes config' do
      let(:routes) { { root: 'foo/bar', fizz: 'buzz/bizz' } }
      subject { request.route_slices('fizz') }
      it { is_expected.to eq %w[buzz bizz] }
    end

    describe 'request method' do
      let(:method) { %w[POST GET PUT PATCH DELETE].sample.downcase }
      let(:env) { { 'REQUEST_METHOD' => method } }
      subject { request.method }
      it { is_expected.to eq method }
    end
  end
end
