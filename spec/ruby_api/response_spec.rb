# frozen_string_literal: true

module RubyAPI
  RSpec.describe Response do
    describe 'class' do
      context 'constants' do
        let(:default_headers) do
          {
            'Content-Type' => 'application/json; charset=utf-8'
          }
        end

        it { expect(described_class::OK).to eq 200 }
        it { expect(described_class::FAIL).to eq 500 }
        it do
          expect(described_class::DEFAULT_HEADERS).to eq(default_headers)
        end
      end
    end

    describe '.error' do
      let(:data) { { foo: :bar } }
      subject { described_class.error data }

      it { is_expected.to be_a described_class }
      it { expect(subject.failure?).to be true }
      it { expect(subject.data).to eq data.to_json }
      it { expect(subject.status).to eq described_class::FAIL }
    end

    describe '.exception' do
      let(:error) { 'foo' }
      let(:exception) { StandardError.new error }
      let(:data) { { message: error, backtrace: exception.backtrace } }
      subject { described_class.exception exception }

      it { is_expected.to be_a described_class }
      it { expect(subject.failure?).to be true }
      it { expect(subject.data).to eq data.to_json }
      it { expect(subject.status).to eq described_class::FAIL }
    end

    describe '.ok' do
      let(:data) { { buzz: :fizz } }
      subject { described_class.ok data }

      it { is_expected.to be_a described_class }
      it { expect(subject.success?).to be true }
      it { expect(subject.data).to eq data.to_json }
      it { expect(subject.status).to eq described_class::OK }
    end

    describe '.operation' do
      let(:data) { { buzz: :fizz } }
      let(:result) do
        Trailblazer::Operation::Railway::Result.new(
          true, { 'result' => data }, {}
        )
      end
      subject { described_class.operation result }

      it { is_expected.to be_a described_class }
      it { expect(subject.success?).to be true }
      it { expect(subject.data).to eq data.to_json }
      it { expect(subject.status).to eq described_class::OK }
    end
  end
end
