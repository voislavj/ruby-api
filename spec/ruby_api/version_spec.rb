# frozen_string_literal: true

RSpec.describe RubyAPI::VERSION do
  it { is_expected.to eq '1.0.2' }
end
