# frozen_string_literal: true

module RubyAPI
  RSpec.describe Operation do
    describe 'class' do
      subject { described_class }
      context 'extends Trailblazer::Operation' do
        it { is_expected.to be < Trailblazer::Operation }
      end
    end

    describe '.process' do
      let(:args) { {} }
      let(:logger) do
        double(:logger)
      end
      let(:app) do
        double(:app).tap do |app|
          allow(app).to receive(:logger).with(no_args) { logger }
        end
      end
      let(:req) do
        double(:req).tap do |req|
          allow(req).to receive(:arguments).with(no_args) { args }
        end
      end
      subject { described_class.process app, req }
      context 'success' do
        it { is_expected.to be_a Trailblazer::Operation::Railway::Result }
      end
    end

    describe 'result' do
      let(:opts) { { foo: :bar } }
      let(:data) { { buzz: :fizz } }
      let(:operation) { described_class.new }
      subject { operation.result opts, data }

      context 'returns data' do
        it { is_expected.to eq data }
      end

      context 'adds :result key to opts hash' do
        before { subject }
        it { expect(opts['result']).to eq data }
      end

      context 'adds :result key for non-hash data' do
        let(:data) { :foo }
        let(:key) { described_class.name.split('::').last.underscore }
        before { subject }
        it { expect(opts['result']).to eq(key => data) }
      end
    end
  end
end
