# frozen_string_literal: true

require 'simplecov'
if ENV['COVERAGE']
  SimpleCov.start do
    add_filter '/spec'
  end
end
