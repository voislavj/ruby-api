# frozen_string_literal: true

ENV['RACK_ENV'] = 'test'

require_relative 'support/simplecov'
require_relative 'support/rspec'
require_relative 'support/ruby_api'
