# frozen_string_literal: true

RSpec.describe RubyAPI do
  let(:root) { '/path/to/root' }

  describe 'module constants' do
    it { expect(described_class::CONFIG_DIR).to eq 'config' }
    it { expect(described_class::CONFIG_FILE).to eq 'app.yml' }
    it { expect(described_class::BOOT_SCRIPT).to eq 'boot' }
  end

  describe '.application' do
    let(:app_path) { 'foo/bar' }
    let(:config_file) { 'buzz/fizz' }
    let(:config_file_path) { File.join root, config_file }
    let(:overrides) { { config_file: config_file } }
    let(:config) do
      {
        app: { source: app_path }
      }
    end
    subject { described_class.application(root, overrides) }

    before do
      allow(File).to receive(:exist?).with(config_file_path) { true }
      allow(YAML).to receive(:load_file).with(config_file_path) { config }
      allow_any_instance_of(described_class::Application)
        .to receive(:require)
        .with(File.join(root, app_path))
    end

    context 'first time creates application' do
      it { is_expected.to be_a described_class::Application }
    end
  end

  describe '.config' do
    let(:config) do
      RubyAPI.path(
        File.join(described_class::CONFIG_DIR, described_class::CONFIG_FILE)
      )
    end
    subject { described_class.config }
    before do
      RubyAPI.root = root
      allow(File).to receive(:exist?).with(config) { true }
      allow(YAML).to receive(:load_file).with(config) { {} }
    end
    context 'success' do
      it { is_expected.to be_a described_class::Config }
    end
  end

  describe 'root' do
    subject { described_class.root }
    before { described_class.root = :foo }

    context 'is equal to :foo' do
      it { is_expected.to eq :foo }
    end
  end

  describe '.environment' do
    subject { described_class.environment }

    context 'loads from ENV' do
      it { is_expected.to eq ENV['RACK_ENV'] }
    end

    context 'defaults to development' do
      before do
        @orig_env = ENV['RACK_ENV']
        ENV['RACK_ENV'] = nil
      end
      after { ENV['RACK_ENV'] = @orig_env }
      it { is_expected.to eq 'development' }
    end
  end

  describe '.development?' do
    subject { described_class.development? }

    context 'in test' do
      it { is_expected.to be true }
    end

    context 'in production' do
      before do
        @orig_env = ENV['RACK_ENV']
        ENV['RACK_ENV'] = 'production'
      end
      after { ENV['RACK_ENV'] = @orig_env }
      it { is_expected.to be false }
    end
  end
end
