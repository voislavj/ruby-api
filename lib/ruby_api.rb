# frozen_string_literal: true

require 'json'
require 'rubygems'
require 'bundler'
require 'trailblazer/operation'
require 'active_support/core_ext/string'
require 'dry-validation'
require 'dry-struct'

# RubyAPI
module RubyAPI
  require_relative 'ruby_api/version'
  require_relative 'ruby_api/config'
  require_relative 'ruby_api/operation'
  require_relative 'ruby_api/response'
  require_relative 'ruby_api/request'
  require_relative 'ruby_api/application'

  CONFIG_DIR = 'config'
  CONFIG_FILE = 'app.yml'
  BOOT_SCRIPT = 'boot'

  def self.application(root, overrides = {})
    @root = root
    Application.new(config(overrides)).boot
  end

  def self.config(overrides = {})
    path = overrides[:config_file]
    path ||= File.join CONFIG_DIR, CONFIG_FILE
    path = path(path)
    raise "Config missing at #{path}" unless File.exist?(path)
    Config.new YAML.load_file(path)
  end

  def self.root=(path)
    @root = path
  end

  def self.root
    @root
  end

  def self.path(path)
    File.join @root, path
  end

  def self.environment
    ENV['RACK_ENV'] || 'development'
  end

  def self.development?
    %w[development test].include? environment
  end
end
