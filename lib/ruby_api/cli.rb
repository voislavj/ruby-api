# frozen_string_literal: true

require 'thor'

module RubAPI
  # RubyAPI CLI Commands
  module Cli
    require_relative 'cli/main'
  end
end
