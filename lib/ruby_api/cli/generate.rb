# frozen_string_literal: true

require_relative '../../ruby_api'

module RubyAPI
  module Cli
    # :nodoc
    class Generate < Thor
      desc 'feature NAME', 'generate application feature module'
      def feature(name)
        path = File.join RubyAPI::CONFIG_DIR, RubyAPI::CONFIG_FILE
        config = RubyAPI.config
        puts config.to_yaml
      end
    end
  end
end
