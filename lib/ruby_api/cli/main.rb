# frozen_string_literal: true

require_relative 'server'
require_relative 'generate'

module RubyAPI
  module Cli
    # RubyAPI Main CLI command
    class Main < Thor
      desc 'server COMMAND', 'control API server'
      subcommand 'server', Server

      desc 'generate OBJECT [OPTIONS]', 'generate framework objects'
      subcommand 'generate', Generate
    end
  end
end
