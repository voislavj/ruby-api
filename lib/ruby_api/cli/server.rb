# frozen_string_literal: true

module RubyAPI
  module Cli
    # :nodoc
    class Server < Thor
      desc 'start', 'start API server'
      option :server, aliases: [:s], type: :string, default: 'puma'
      option :port,   aliases: [:p], type: :numeric, default: 1234
      option :config, aliases: [:c], type: :string, default: 'config.ru'
      def start
        exec format('bundle exec %<server>s -p %<port>d %<config>s',
                    server: options[:server],
                    port: options[:port],
                    config: options[:config])
      end
    end
  end
end
