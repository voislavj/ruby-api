# frozen_string_literal: true

module RubyAPI
  # Operation Error
  class Operation < Trailblazer::Operation
    def self.process(app, request)
      call(request.arguments.merge(logger: app.logger))
    end

    def result(opts, data)
      data = { class_key => data } unless data.is_a?(Hash)
      opts['result'] = data
    end

    private

    def class_key
      self.class.name.split('::').last.underscore
    end
  end
end
