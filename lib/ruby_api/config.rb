# frozen_string_literal: true

module RubyAPI
  # Config data
  class Config
    def self.from_file(path)
      new(YAML.load_file(path))
    end

    def initialize(data)
      @data = prep_data(data)
    end

    def merge!(data)
      @data.merge! prep_data(data)
    end

    def key?(key)
      @data.key?(key)
    end

    def respond_to_missing?(name, *)
      @data.key?(name)
    end

    def method_missing(name, *args)
      return @data[name.to_sym] if respond_to_missing?(name.to_sym)
      super
    end

    private

    def prep_data(hash)
      hash.deep_symbolize_keys!
    end
  end
end
