# frozen_string_literal: true

module RubyAPI
  # Response
  class Response
    FAIL = 500
    OK = 200

    DEFAULT_HEADERS = {
      'Content-Type' => 'application/json; charset=utf-8'
    }.freeze

    class << self
      def error(data)
        new(data, error: true)
      end

      def exception(err)
        data = { message: err.message }
        data[:backtrace] = err.backtrace if RubyAPI.development?
        new(data, error: true)
      end

      def ok(data)
        new(data)
      end

      def operation(result)
        return new(result['errors'], error: true) if result.failure?
        new(result['result'])
      end
    end

    def status
      success? ? OK : (@status || FAIL)
    end

    def headers
      @headers || DEFAULT_HEADERS
    end

    def failure?
      @error
    end

    def success?
      !failure?
    end

    def data
      @data.is_a?(String) ? @data : @data.to_json
    end

    private

    def initialize(data, error: false)
      @data = data
      @error = error ? true : false
    end
  end
end
