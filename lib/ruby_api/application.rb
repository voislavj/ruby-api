# frozen_string_literal: true

module RubyAPI
  # Application
  class Application
    APP_MODULE_NAME = 'APP'
    APP_SOURCE_PATH = 'src/main'

    attr_reader :config, :logger, :routes

    def initialize(config)
      raise "Invalid config: #{config.inspect}" unless config.is_a?(Config)
      @config = config
      @routes = load_routes
    end

    def load_routes
      return {} unless config.key?(:routes)

      routes = config.routes
      require_hash_or_string(routes, 'Invalid routes config: %s')
      routes = YAML.load_file RubyAPI.path(routes) if routes.is_a?(String)
      @routes = routes.deep_symbolize_keys!
    end

    def boot
      @logger = Logger.new log_path[:info]
      @logger_err = Logger.new log_path[:error]
      require RubyAPI.path(boot_script) if boot_script?
      require RubyAPI.path(app_path)
      self
    end

    def call(env)
      @env = env
      response = process Request.new(self, env, routes)
      return handle_error(response) if response.failure?
      handle_success(response)
    rescue StandardError => e
      handle_exception(e)
    end

    def app_module
      Kernel.const_get(app_name)
    end

    private

    def log_path
      if config.key?(:log)
        { info: config.log[:info], error: config.log[:error] }
      else
        { info: $stdout, error: $stderr }
      end
    end

    def app_name
      name = config.app[:name] if config.key?(:app) && config.app[:name]
      name || APP_MODULE_NAME
    end

    def app_path
      return APP_SOURCE_PATH unless config.key?(:app)
      return APP_SOURCE_PATH unless config.app.key?(:source)
      config.app[:source]
    end

    def boot_script
      config.boot_script
    end

    def boot_script?
      return false unless config.key?(:boot_script)
      path = RubyAPI.path(boot_script)
      return true if File.exist?(path)
      logger.warn "Boot script not found at: #{path}"
      false
    end

    def process(request)
      operation = request.operation
      result = operation.process(self, request)
      Response.operation(result)
    end

    def handle_success(response)
      rack_response response.status, response.headers, response.data
    end

    def handle_error(response)
      @logger_err.error response.data
      rack_response Response::OK, response.headers, response.data
    end

    def handle_exception(error)
      @logger_err.error error
      rack_response Response::FAIL, Response::DEFAULT_HEADERS,
                    Response.exception(error).data
    end

    def rack_response(status, headers, data)
      data = [data] unless data.is_a?(Array)
      [status, headers || {}, data]
    end

    def require_hash_or_string(object, message = 'Invalid Hash/String: %s')
      return if object.is_a?(Hash) || object.is_a?(String)
      raise format(message, object.inspect)
    end
  end
end
