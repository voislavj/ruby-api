# frozen_string_literal: true

require 'rack'
require 'rack/multipart'
require 'rack/request'

module RubyAPI
  # Request
  class Request
    def initialize(app, env, routes = {})
      @app = app
      @env = env
      @routes = routes
      @rack_req = Rack::Request.new(env)
    end

    def feature
      class_name = feature_name.camelize
      unless app_module.const_defined?(class_name)
        raise "Feature `#{feature_name}` not found"
      end
      app_module.const_get(class_name)
    end

    def operation
      class_name = operation_name.camelize
      unless feature.const_defined?(class_name)
        fname = feature.name.split('::').last.underscore
        raise "Operation `#{fname}/#{operation_name}` not found"
      end
      feature.const_get class_name
    end

    def arguments
      @rack_req.params.merge json_arguments
    end

    def json_arguments
      JSON.parse @rack_req.body.read
    rescue JSON::ParserError
      {}
    end

    def app_module
      @app.app_module
    end

    def root_route
      @routes[:root]
    end

    def root_feature
      root_route.split('/')[0].to_s
    end

    def root_operation
      root_route.split('/')[1].to_s
    end

    def feature_name
      slices[0].to_s.empty? ? root_feature : slices[0].to_s
    end

    def operation_name
      slices[1].to_s.empty? ? root_operation : slices[1].to_s
    end

    def slices
      return route_slices(path) if routes?(path)
      path.split('/')
    end

    def path
      @env['REQUEST_PATH'].to_s.gsub(%r{^\/}, '')
    end

    def routes?(path)
      @routes.key?(path.to_sym)
    end

    def route_slices(path)
      @routes[path.to_sym].split('/')
    end

    def method
      @env['REQUEST_METHOD'].to_s.downcase
    end
  end
end
